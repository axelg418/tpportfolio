var brightToDark = ["#FFFFFF", "#F8FAFC", "#F1F5F8", "#DAE1E7", "#B8C2CC", "#8795A1", "#606F7B", "#3D4852", "#22292F"];
var darkToBright = ["#22292F",  "#3D4852",  "#606F7B", "#8795A1",  "#B8C2CC",  "#DAE1E7", "#F1F5F8", "#F8FAFC", "#FFFFFF"];
var mode = "dayMode";

$("document").ready(function(){
    

   $("#button").click(function(){
       if(mode == "dayMode"){
           changeBackground(brightToDark);
       }
       else{
           changeBackground(darkToBright);
       }
    });
    
    function changeBackground(tab){
        var i = 0;
        $("#button").css("visibility", "hidden");
        var interval = setInterval(function(){
            $("body").css("background-color", tab[i]);            
            i++;
            if (i == tab.length){
                if(mode == "dayMode"){
                    $("#button").css("background-color", "black");
                    $("#button").css("color", "white");
                    $("#button").text("mode jour");
                    $("body").css("color", "white");
                    mode = "nightMode";
                }else{
                    $("#button").css("background-color", "#e7e7e7");
                    $("#button").css("color", "black");
                    $("#button").text("mode nuit");
                    $("body").css("color", "black");
                    mode = "dayMode";
                }
                clearInterval(interval);
                $("#button").css("visibility", "visible");
            }
            }, 500);
        
    }

});

